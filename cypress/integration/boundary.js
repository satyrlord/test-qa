/// <reference types="Cypress" />

context('Terms', () => {
  beforeEach(() => {
    cy.visit('http://test.getgrex.com/')
  })
  describe('Verify positive scenario', function() {
	it('Calculates 10!', function() {
		cy.get('#number').type('10')
		cy.server()
		cy.route('POST', '/factorial').as('getFactorial')	
		cy.get('#getFactorial').click()
		cy.wait('@getFactorial').its('status').should('eq', 200)			
		cy.get('#resultDiv').contains('3628800') // 10! should be 3628800
	})
  })
	describe('Verify boundaries', function() {
	it('Calculates 0!', function() {
		cy.get('#number').type('0')
		cy.get('#getFactorial').click()
		cy.get('#resultDiv').contains('1') // 0! should be 1
	})
	it('Calculates 1!', function() {
		cy.get('#number').type('1')
		cy.get('#getFactorial').click()
		cy.get('#resultDiv').contains('1') // 1! should be 1
	})
  })
    describe('Verify negative scenarios', function() {
	it('Enters blank', function() {
		cy.get('#getFactorial').click()
		cy.get('#resultDiv').contains('Please enter an integer')
	})
	it('Enters a letter', function() {
		cy.get('#number').type('a')
		cy.get('#getFactorial').click()
		cy.get('#resultDiv').contains('Please enter an integer')
	})
	it('Enters a negative number', function() {
		cy.get('#number').type('-1')
		cy.server()
		cy.route('POST', '/factorial').as('getFactorial')	
		cy.get('#getFactorial').click()
		cy.wait('@getFactorial').its('status').should('eq', 200)
	})
  })
})