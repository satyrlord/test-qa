/// <reference types="Cypress" />

context('Terms', () => {
  beforeEach(() => {
    cy.visit('http://test.getgrex.com/')
  })
  describe('Verify terms and conditions', function() {
	it('Clicks on the Terms link', function() {
		cy.contains('Terms').click()
		cy.url().should('include', 'terms') // => true
	})
  })
})