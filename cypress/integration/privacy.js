/// <reference types="Cypress" />

context('Terms', () => {
  beforeEach(() => {
    cy.visit('http://test.getgrex.com/')
  })
  describe('Verify privacy', function() {
	it('Clicks on the Privacy link', function() {
		cy.contains('Privacy').click()
		cy.url().should('include', 'privacy') // => true
	})
  })
})